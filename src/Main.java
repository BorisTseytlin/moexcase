import java.io.*;
import java.util.*;
import java.util.regex.Pattern;

public class Main {

    private static final String inputFilePath = "input.csv";
    private static long auctionPrice;
    public static ArrayList<Order> buyMarketOrders = new ArrayList<Order>(200000);     // preallcoate memory for speed
    public static ArrayList<Order> sellMarketOrders = new ArrayList<Order>(200000);
    public static ArrayList<Order> buyLimitOrders = new ArrayList<Order>(200000);
    public static ArrayList<Order> sellLimitOrders = new ArrayList<Order>(200000);

    static long totalSellMarketVolume = 0;
    static long totalBuyMarketVolume = 0;

    public static TreeMap<Long, priceLevel> buyPrices = new TreeMap<Long, priceLevel>();  //treemaps for sorted order, key 1 is price, key 2 is price level
    public static TreeMap<Long, priceLevel> sellPrices = new TreeMap<Long, priceLevel>();

    public static long totalMoney = 0;

    static long marketSellOrders = 0;
    private static long corruptedLines = 0;
    private static long greedyOrders = 0;
    public static void main(String[] args) {

        final long startTime = System.currentTimeMillis();
        ReadOrdersFromInputFile(inputFilePath);

        if (AuctionIsValid()){
            auctionPrice = CalculateAuctionPrice();
            MatchDeals();
        }
        else {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream("output.csv"), "utf-8"));
                writer.write("FAILED");

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                try {writer.close();} catch (Exception e) {e.printStackTrace();}
            }
        }

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("report.txt"), "utf-8"));
            writer.write("Total execution time: " + (System.currentTimeMillis() - startTime));
            writer.newLine();
            writer.write("Corrupted input lines found and ignored: " + corruptedLines);
            writer.newLine();
            writer.write("Greedy orders found and ignored: " + greedyOrders);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {writer.close();} catch (Exception e) {e.printStackTrace();}
        }
    }

    private static boolean AuctionIsValid() {
        if (sellLimitOrders.size() >0 && buyLimitOrders.size() > 0  &&
                buyLimitOrders.get(0).getPrice() >= sellLimitOrders.get(0).getPrice()){
                    return true;

        }
        return false;
    }

    private static long CalculateAuctionPrice() {
        long bestPrice = 0;
        long bestTotalMoney = 0;

        long totalVolume = 0;

        long levelTotalVolume;

        boolean finish = false;
        for (Map.Entry<Long, priceLevel> buyPrice : buyPrices.descendingMap().entrySet() ){

            for (Map.Entry<Long, priceLevel> sellPrice : sellPrices.entrySet() ){
                if (buyPrice.getKey() >= sellPrice.getKey()){
                    buyPrice.getValue().addMatchedVolume(sellPrice.getValue().getTotalVolume());
                }
                else {
                    break;
                }
            }
            buyPrice.getValue().addMatchedVolume(totalSellMarketVolume);// we can satisfy all market orders too
            levelTotalVolume = totalVolume;

            if (totalVolume + buyPrice.getValue().getTotalVolume()+totalBuyMarketVolume < buyPrice.getValue().getMatchedVolume()){
                levelTotalVolume += buyPrice.getValue().getTotalVolume()+totalBuyMarketVolume;
            }
            else {
                levelTotalVolume = buyPrice.getValue().getMatchedVolume();
                finish = true;
            }
            if (levelTotalVolume * buyPrice.getKey() > bestTotalMoney){
                bestTotalMoney = levelTotalVolume * buyPrice.getKey();
                bestPrice = buyPrice.getKey();
            }

            totalVolume += buyPrice.getValue().getTotalVolume();

            if (finish) {
                break;
            }
        }
        // where these volumes overlap, matching volume
        totalMoney = bestTotalMoney;
        return bestPrice;
    }

    private static void MatchDeals() {
        int i = 0;
        int j = 0;
        long dealVolume;
        long dealMoney;
        Order buyorder;
        Order sellorder;


        long bsize = buyMarketOrders.size()+buyLimitOrders.size();
        long ssize = sellMarketOrders.size()+sellLimitOrders.size();

        long bquantity;
        long squantity;

        boolean greedyorder = false;

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(
                    new FileOutputStream("output.csv"), "utf-8"));
            writer.write("OK," + auctionPrice  + "," + totalMoney);

            while( i < bsize && j < ssize ){
                greedyorder = false;
                if (i >= buyMarketOrders.size()){
                    buyorder = buyLimitOrders.get(i-buyMarketOrders.size());
                }
                else {
                    buyorder = buyMarketOrders.get(i);
                }
                if (j >= sellMarketOrders.size()){
                    sellorder = sellLimitOrders.get(j-sellMarketOrders.size());
                }
                else {
                    sellorder = sellMarketOrders.get(j);
                }

                if (!buyorder.isMarketOrder() && buyorder.getPrice() < auctionPrice){
                    i++;
                    greedyOrders++;
                    greedyorder = true;
                }
                if (!sellorder.isMarketOrder() && sellorder.getPrice() > auctionPrice){
                    j++;
                    greedyOrders++;
                    greedyorder = true;
                }
                if (greedyorder){
                    continue;
                }
                bquantity = buyorder.getQuantity();
                squantity = sellorder.getQuantity();

                if (bquantity > squantity){
                    dealVolume = squantity;
                }
                else if (bquantity < squantity) {
                    dealVolume = bquantity;
                }
                else {
                    dealVolume = squantity;
                }
                dealMoney = dealVolume * auctionPrice;
                writer.newLine();
                writer.write(buyorder.getNumber()
                        + "," + sellorder.getNumber()
                        + "," + dealMoney
                        + "," + dealVolume
                );
                if (bquantity - dealVolume < 1){
                    i++;
                }else{
                    buyorder.setQuantity(bquantity - dealVolume);
                }
                if (squantity - dealVolume < 1){
                    j++;
                }
                else {
                    sellorder.setQuantity(squantity - dealVolume);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {writer.close();} catch (Exception e) {e.printStackTrace();}
        }
    }

    public static void ReadOrdersFromInputFile(String filepath){
         File file = new File(filepath);
         try {
             Scanner sc = new Scanner(file);
             long number;
             long price;
             long quantity;
             boolean isMarketOrder;     // market or limit
             boolean isBuyOrder; // buyOrSell
             Pattern validation;
             String curLine;
             String[] segments = new String[5];
             char delimeter;
             if (sc.hasNext()){
                 curLine = sc.next();// remove header line
                 delimeter = curLine.charAt(5);
                 // precompile pattern for speed
                 validation = Pattern.compile("(\\d+"+delimeter+"[B|S]"+delimeter+"[M|L]"+delimeter+"\\d+("+delimeter+"\\d+|$|"+delimeter+"$))");
                 while(sc.hasNext()){
                    curLine = sc.next();

                    if (!validation.matcher(curLine).matches()){
                        corruptedLines++;
                        continue;
                    }
                    segments = curLine.split(delimeter+"");
                    number = Integer.parseInt(segments[0]);
                    isBuyOrder = segments[1].equals("B");
                    isMarketOrder = segments[2].equals("M");
                    quantity = Integer.parseInt(segments[3]);
                    if (isMarketOrder) {
                        price = 0; // for market orders we ignore the price, set to 0
                        if (isBuyOrder){
                            totalBuyMarketVolume += quantity;
                            buyMarketOrders.add(new Order(number, isMarketOrder, isBuyOrder, quantity, price));
                        }
                        else {
                            totalSellMarketVolume += quantity;
                            sellMarketOrders.add(new Order(number, isMarketOrder, isBuyOrder, quantity, price));
                        }
                        marketSellOrders++;
                    }
                    else{  // if limit order
                        try{
                            price = Integer.parseInt(segments[4]);
                        }
                        catch (ArrayIndexOutOfBoundsException e){
                            corruptedLines++;
                            continue;
                        }

                        if (isBuyOrder){
                            // if such price level doesn't already exist
                            if (!buyPrices.containsKey(price)){
                                buyPrices.put(price, new priceLevel(price, quantity));
                            }
                            // else add vol to current price level
                            else {
                                buyPrices.get(price).addTotalVolume(quantity);
                            }
                            buyLimitOrders.add(new Order(number, isMarketOrder, isBuyOrder,quantity, price));
                        }
                        else {
                            if (! sellPrices.containsKey(price)) {
                               sellPrices.put(price, new priceLevel(price, quantity));
                            }
                            else {
                                sellPrices.get(price).addTotalVolume(quantity);
                            }
                            sellLimitOrders.add(new Order(number, isMarketOrder,isBuyOrder,quantity, price));
                        }

                    }
                 }
             }
             Collections.sort(buyLimitOrders);
             Collections.sort(sellLimitOrders);

             sc.close();
         }
         catch( FileNotFoundException e ){
             e.printStackTrace();
         }
    }
}

class Order implements Comparable<Order>{
    private long number;
    private long price;
    private long quantity;
    private boolean isMarketOrder;
    private boolean isBuyOrder;

    public Order(long num, boolean isMOrder, boolean isBOrder, long quant, long pr){
        number = num;
        isMarketOrder = isMOrder;
        isBuyOrder = isBOrder;
        quantity = quant;
        price = pr;
    }


    public long getPrice(){

        return price;
    }
    public long getQuantity(){
        return quantity;
    }

    public void setPrice(long num){
        price = num;
    }

    public void setQuantity(long num){
        quantity = num;
    }

    public boolean isMarketOrder() {
        return isMarketOrder;
    }

    public void setMarketOrder(boolean marketOrder) {
        isMarketOrder = marketOrder;
    }

    public boolean getBuyOrder() {
        return isBuyOrder;
    }

    public void setBuyOrder(boolean buyOrder) {
        isBuyOrder = buyOrder;
    }

    public long getNumber() {
        return number;
    }

    public void setNumber(long number) {
        this.number = number;
    }

    @Override
    public int compareTo(Order o) {
        int value2 = Long.compare(this.getPrice(),(o.getPrice()));
        if (value2 != 0) {
            if (this.isBuyOrder) {
                return -value2;
            }
            else {
                return value2;
            }
        }
        int value3 =  Long.compare(this.getNumber(),(o.getNumber()));
        return value3;
    }
}

class Deal {
    private long volume;
    private long totalmoney;
    private long buyOrderNumber;
    private long sellOrderNumber;

    Deal(long buyOrderNumber, long sellOrderNumber, long totalmoney, long volume) {
        this.buyOrderNumber = buyOrderNumber;
        this.sellOrderNumber = sellOrderNumber;
        this.totalmoney = totalmoney;
        this.volume = volume;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long volume) {
        this.volume = volume;
    }

    public long getTotalmoney() {
        return totalmoney;
    }

    public void setTotalmoney(long totalmoney) {
        this.totalmoney = totalmoney;
    }

    public long getBuyOrderNumber() {
        return buyOrderNumber;
    }

    public void setBuyOrderNumber(long buyOrderNumber) {
        this.buyOrderNumber = buyOrderNumber;
    }

    public long getSellOrderNumber() {
        return sellOrderNumber;
    }

    public void setSellOrderNumber(long sellOrderNumber) {
        this.sellOrderNumber = sellOrderNumber;
    }
}


class priceLevel {
    private long totalVolume;
    private long matchedVolume;
    private long price;
    priceLevel(long pr, long vol){
        this.price = pr;
        this.totalVolume = vol;
    }

    public long getTotalVolume() {
        return totalVolume;
    }

    public void setTotalVolume(long totalVolume) {
        this.totalVolume = totalVolume;
    }
    public void addTotalVolume(long totalVolumeToAdd) {
        this.totalVolume += totalVolumeToAdd;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getMatchedVolume() {
        return matchedVolume;
    }

    public void addMatchedVolume(long matchedVolumeToAdd) {
        this.matchedVolume += matchedVolumeToAdd;
    }
    public void setMatchedVolume(long matchedVolume) {
        this.matchedVolume = matchedVolume;
    }
}